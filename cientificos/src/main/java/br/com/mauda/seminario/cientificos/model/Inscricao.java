package br.com.mauda.seminario.cientificos.model;

import java.util.logging.Logger;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private static final Logger LOG = Logger.getLogger(Inscricao.class.getName());
    private Long id;
    private Boolean direitoMaterial;
    private Seminario seminario;
    private SituacaoInscricaoEnum situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;
    private Estudante estudante;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
    }

    public void realizarCheckIn() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacaoInscricaoEnum)) {
            this.situacaoInscricaoEnum = SituacaoInscricaoEnum.CHECKIN;
        } else {
            LOG.severe("Inscrição nao comprada ou seminario ja aconteceu!!");
        }
    }

    public void cancelarCompra() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacaoInscricaoEnum)) {
            this.situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;
            this.estudante.removeInscricao(this);
            this.estudante = null;
            this.direitoMaterial = null;
        } else {
            LOG.severe("Não é possível cancelar a compra!!");
        }
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        if (SituacaoInscricaoEnum.DISPONIVEL.equals(this.situacaoInscricaoEnum)) {
            this.direitoMaterial = direitoMaterial;
            this.situacaoInscricaoEnum = SituacaoInscricaoEnum.COMPRADO;
            this.estudante = estudante;
            this.estudante.adicionarInscricao(this);
        }
    }

    public Long getId() {
        return this.id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacaoInscricaoEnum;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
