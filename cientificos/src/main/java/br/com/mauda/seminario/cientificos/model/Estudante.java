package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Estudante {

    private static final Logger LOG = Logger.getLogger(Estudante.class.getName());
    private Long id;
    private String nome;
    private String telefone;
    private String email;
    private Instituicao instituicao;
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Estudante(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void adicionarInscricao(Inscricao inscricao) {
        if (this.possuiInscricao(inscricao)) {
            LOG.severe("Já possui essa inscrição!!");
        } else {
            this.inscricoes.add(inscricao);
        }
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public void removeInscricao(Inscricao inscricao) {
        if (this.inscricoes.contains(inscricao)) {
            this.inscricoes.remove(inscricao);
        }

    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
