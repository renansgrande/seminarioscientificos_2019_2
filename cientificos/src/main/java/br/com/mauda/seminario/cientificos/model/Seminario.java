package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario {

    private Long id;
    private String titulo;
    private String descricao;
    private boolean mesaRedonda;
    private Date data;
    private Integer qtdInscricoes;
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    private Seminario(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
        for (int i = 0; i < this.qtdInscricoes; i++) {
            this.inscricoes.add(new Inscricao(this));
        }
    }

    public Seminario(List<AreaCientifica> areasCientificas, List<Professor> professores, Integer qtdInscricoes) {
        this(qtdInscricoes);
        this.professores = professores;
        for (Professor professor : this.professores) {
            professor.adicionarSeminario(this);
        }
        this.areasCientificas = areasCientificas;
    }

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        this(qtdInscricoes);
        professor.adicionarSeminario(this);
        this.professores.add(professor);
        this.areasCientificas.add(areaCientifica);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }
}
